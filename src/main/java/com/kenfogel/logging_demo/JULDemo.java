package com.kenfogel.logging_demo;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Logging Demo This demo uses the Java SE java.util.logging library
 *
 */
public class JULDemo {

    private static final Logger LOG;

    // Set up the java.util.logging with a custom logging.properties to replace 
    // the default one. On my system it is at 
    // C:\Program Files\AdoptOpenJDK\jdk-14.0.2.12-hotspot\conf
    // Changes to this config file will be used by JUL if the following code that
    // loads an application specific config is not used.
    static {

        try (InputStream is = retrieveURLOfJarResource("logging.properties").openStream()) {
            LogManager.getLogManager().readConfiguration(is);
        } catch (Exception e) {
            Logger.getAnonymousLogger().severe("Umable to load logging.properties\nProgram is exiting");
            System.exit(1);
        }
        LOG = Logger.getLogger(JULDemo.class.getName());
    }

    /**
     * This supports accessing the logging.properties file stored in the
     * resource folder of the jar file
     *
     * @param resourceName
     * @return
     */
    public static java.net.URL retrieveURLOfJarResource(String resourceName) {
        return Thread.currentThread().getContextClassLoader().getResource(resourceName);
    }

    public JULDemo() {
        super();
    }

    /**
     * LOG.log takes the level for param1, the message for param2, and
     * optionally a third param for the Exception object. LOG.level such as
     * LOG.severe only takes one param and it is the message
     */
    public void perform() {
        // Choice #1 for using the LOG where level is a parameter
        LOG.log(Level.FINEST, "I am a LOG.log(Level.FINEST, \". . . \") ");
        LOG.log(Level.FINER, "I am a LOG.log(Level.FINER, \". . . \") ");
        LOG.log(Level.FINE, "I am a LOG.log(Level.FINE, \". . . \") ");
        LOG.log(Level.INFO, "I am a LOG.log(Level.INFO, \". . . \") ");
        LOG.log(Level.WARNING, "I am a LOG.log(Level.WARNING, \". . . \") ");
        LOG.log(Level.SEVERE, "I am a LOG.log(Level.SEVERE, \". . . \") ");

        // Choice #2 for using the LOG where level is the method name
        LOG.finest("I am a LOG.finest(\". . . \") ");
        LOG.finer("I am a LOG.finer(\". . . \") ");
        LOG.fine("I am a LOG.fine(\". . . \") ");
        LOG.info("I am a LOG.info(\". . . \") ");
        LOG.warning("I am a LOG.warning(\". . . \") ");
        LOG.severe("I am a LOG.severe(\". . . \") ");
    }

    public static void main(String[] args) throws InterruptedException {
        JULDemo app = new JULDemo();
        app.perform();
        System.exit(0);
    }
}
