package com.kenfogel.logging_demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logging Demo Review the log4j.xml file found in the src/main/resources folder
 * The LOG output will appear in the console and in a file. You will find the
 * file by switching to the Files view in NetBeans and then locate the 'log'
 * folder just below the root folder.
 *
 */
public class Log4jDemo {

    // Declare a private or protected Logger object initialized with the class's
    // name
    private final static Logger LOG = LoggerFactory.getLogger(Log4jDemo.class);

    public Log4jDemo() {
        super();
    }

    /**
     * All LOG methods are overloaded to accept a second parameter of the
     * exception object if required.
     */
    public void perform() {
        LOG.trace("Level 1: I am a trace");
        LOG.debug("Level 2: I am a debug");
        LOG.info("Level 3: I am an info");
        LOG.warn("Level 4: I am a warning");
        LOG.error("Level 5: I am an error");
    }

    public static void main(String[] args) {
        Log4jDemo app = new Log4jDemo();
        app.perform();
        System.exit(0);
    }
}
